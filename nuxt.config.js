export default {
  mode: 'universal',
  // mode: 'spa',
  /*
  // eslint-disable-next-line prettier/prettier
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {
      src: '~plugins/vue-fa'
    },
    {
      src: '~plugins/vue-validation',
      mode: 'client'
    }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.API_URL || 'http://localhost/fiama' // API Endpoint // https://webdemo.letschbang.co.in/fiama // http://localhost/fiama
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          // login: {
          //   url: '/wp-json/custom/v2/login',
          //   method: 'post',
          // },
          login: {
            url: '/wp-json/jwt-auth/v1/token ',
            method: 'post',
            propertyName: 'token'
          },
          logout: { url: '/api/auth/logout', method: 'post' },
          // user: {
          //   url: '/wp-json/wp/v2/users',
          //   method: 'get',
          //   propertyName: 'user'
          // }
          user: {
            url: '/wp-json/custom/v2/user',
            method: 'get',
            propertyName: 'user'
          }
        },
        // tokenRequired: true,
        tokenType: ''
        // globalToken: true,
        // autoFetchUser: true
      }
    }
  }
}
