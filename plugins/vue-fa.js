import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import {
  faAmazon,
  faFacebookF,
  faInstagram,
  faTwitter,
  faYoutube,
  faWhatsapp,
  faInstagramSquare
} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faTimes,
  faPlay,
  faUserCircle,
  faArrowRight,
  faSearch,
  faChevronLeft,
  faChevronRight,
  faChevronDown
} from '@fortawesome/free-solid-svg-icons'

config.autoAddCss = false

library.add(faAmazon)
library.add(faFacebookF)
library.add(faInstagram)
library.add(faInstagramSquare)
library.add(faTwitter)
library.add(faYoutube)
library.add(faWhatsapp)
library.add(faTimes)
library.add(faPlay)
library.add(faUserCircle)
library.add(faArrowRight)
library.add(faSearch)
library.add(faChevronLeft)
library.add(faChevronRight)
library.add(faChevronDown)

Vue.component('fa', FontAwesomeIcon)
