import Vue from 'vue'
import {
  ValidationProvider,
  ValidationObserver,
  extend as vExtend
} from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'

vExtend('required', rules.required)
vExtend('email', rules.email)
vExtend('alpha', rules.alpha)
vExtend('alpha_spaces', rules.alpha_spaces)
vExtend('numeric', rules.numeric)
vExtend('length', rules.length)
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)
