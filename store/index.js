export const state = () => ({
  menus: [],
  users: [],
  currentUser: {}
})

export const mutations = {
  SET_MENUS(state, data) {
    state.menus = data
  },
  SET_USERS(state, users) {
    state.users = users
  },
  LOGOUT_USER(state) {
    state.currentUser = {}
    window.localStorage.currentUser = JSON.stringify({})
  },
  SET_CURRENT_USER(state, user) {
    state.currentUser = user
    window.localStorage.currentUser = JSON.stringify(user)
  }
}

export const actions = {
  async nuxtServerInit({ commit, dispatch }, { req }) {
    const menus = await this.$axios.$get(`/wp-json/menus/v1/menus/2`)
    commit('SET_MENUS', menus)
  },
  async loadUsers({ commit }) {
    const response = await this.$axios.$get('/wp-json/custom/v2/users')
    const users = response.user
    commit('SET_USERS', users)
  },
  async loadCurrentUser({ commit }) {
    const user = await JSON.parse(window.localStorage.currentUser)
    commit('SET_CURRENT_USER', user)
  },
  logoutUser({ commit }) {
    commit('LOGOUT_USER')
  },
  // loginUser({ commit }, user) {
  //   commit('SET_CURRENT_USER', user)
  // }
  async loginUser({ commit }, loginInfo) {
    try {
      const response = await this.$axios.$post(
        `/wp-json/custom/v2/login`,
        loginInfo
      )
      if (response.user) {
        const user = response.user.data
        commit('SET_CURRENT_USER', user)
        return user
      } else {
        return {
          error:
            'Username/password  combination  was incorrect. Please try again'
        }
      }
    } catch {
      return {
        error: 'Username/password  combination  was incorrect. Please try again'
      }
    }
  }
}

export const getters = {
  menuItems(state) {
    const menus = []
    state.menus.items.forEach((item) => {
      if (item.type === 'custom') {
        menus.push({
          id: item.id,
          title: item.title,
          to: item.url,
          smooth: true
        })
      } else {
        menus.push({
          id: item.id,
          title: item.title,
          to: item.slug
        })
      }
    })
    return menus
  }
}
