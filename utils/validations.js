const required = (propertyType) => {
  return (v) => (v && v.length > 0) || `You must input a ${propertyType}`
}
export default {
  required
}
